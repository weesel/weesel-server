<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Database\Seeder;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Storage;

class ProductSeeder extends Seeder
{
    /**
     * The current Faker instance.
     *
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * Create a new seeder instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->faker = $this->withFaker();
    }

    /**
     * Get a new Faker instance.
     *
     * @return \Faker\Generator
     */
    protected function withFaker()
    {
        return Container::getInstance()->make(Generator::class);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Product::factory(1000)->create();

        /**
         * generate discount
         */
        Product::all()->random(40)->each(function ($product) {
            $product->discount = $this->faker->numberBetween(10, 20);
            $product->save();
        });

        Product::all()->each(function ($product) {
            $parentCategories =
                Category::where('parent_id', '=', null)->get()->random(1);
            $parentCategoriesId = $parentCategories->pluck('id');
            $childrenCategories = Category::where('parent_id', '=', $parentCategoriesId[0])->get();
            if (count($childrenCategories) > 0) {
                $childrenCategories = $childrenCategories->random(1, count($childrenCategories))->pluck('id');
            }
            $categories = array_merge($parentCategoriesId->toArray(), $childrenCategories->toArray());
            $product->categories()->attach($categories);
            for ($i = 0; $i < rand(1, 5); $i++) {
                ProductImage::create([
                    'product_id' => $product->id,
                    'url' => 'https://weesel.s3.ap-southeast-1.amazonaws.com/' . $this->faker->randomElement(Storage::disk('s3')->allFiles('images/' . strtolower($parentCategories->pluck('name')[0])))
                ]);
            }
        });

        /**
         * generating discounts for product
         */
        Product::all()->random(100)->each(function ($product) {
            $product->discount = rand(10, 20);
            $product->save();
        });
    }
}
