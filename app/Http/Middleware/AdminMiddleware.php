<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if (!in_array(auth()->user()->roles()->first()->name, ['merchant', 'admin'])) {
            return response()->json([
                'message' => 'Unauthorized',
                'content' => 'Restricted for privileged user only'
            ], 403);
        }
        return $response;
    }
}
