<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Models\Seller;
use Illuminate\Http\Request;

class SellerProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Seller $seller)
    {
        $products = $seller->products();

        /**
         * query builders here
         */
        /**
         * aggregate stats
         */
        if ($limit = $request->query('limit')) {
            $products = $products->take($limit)->get();
        } else {
            $products = $products->paginate(10);
            $products->getCollection()->transform(function ($product) use ($seller) {
                $product['sale_count'] =
                    $seller->orders()->where('status', 'completed')->with([
                        'products' => function ($q) use ($seller, $product) {
                            $q->where([
                                ['id', $product->id],
                                ['seller_id', $seller->id]
                            ]);
                        }
                    ])
                    ->get()
                    ->reduce(function ($total_product_count, $order) {
                        return $total_product_count + count($order->products);
                    }, 0);
                return $product;
            });
        }

        return response()->json([
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function show(Seller $seller)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function edit(Seller $seller)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seller $seller)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller)
    {
        //
    }
}
