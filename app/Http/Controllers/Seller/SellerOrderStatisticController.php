<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Models\Seller;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use DatePeriod;
use Illuminate\Http\Request;

class SellerOrderStatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Seller $seller)
    {
        //
        $orders = $seller->orders();
        $total_revenue_statistic = array();
        if ($date = $request->query('date')) {
            switch ($date) {
                case 'this week':
                    $week = CarbonPeriod::create(Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek());
                    $weekMap = [
                        0 => 'Sunday',
                        1 => 'Monday',
                        2 => 'Tuesday',
                        3 => 'Wednesday',
                        4 => 'Thursday',
                        5 => 'Friday',
                        6 => 'Saturday'
                    ];
                    foreach ($week as $day) {
                        $total_revenue = round($seller->orders()->whereDate('created_at', '=', $week)->get()->reduce(
                            function ($total_revenue, $order) {
                                return $total_revenue + $order['products']->reduce(function ($order_revenue, $product) {
                                    return $order_revenue + ($product->price * ((100 - $product->discount) / 100)) * $product->pivot->order_product_quantity;
                                }, 0);
                            },
                            0
                        ), 2);
                        $total_revenue_statistic[$weekMap[$day->dayOfWeek]] = $total_revenue;
                    }
                    break;
                case 'this month':
                    $month = CarbonPeriod::create(Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth());
                    foreach ($month as $index => $day) {
                        if ($day->weekOfMonth !== $day->subDay()->weekOfMonth) {
                            $total_revenue_statistic['Quarter ' . ($index / 7 + 1)] = round($seller->orders()->whereBetween('created_at', [date($day), date($day->addDays(7))])->get()->reduce(
                                function ($total_revenue, $order) {
                                    return $total_revenue + $order['products']->reduce(function ($order_revenue, $product) {
                                        return $order_revenue + ($product->price * ((100 - $product->discount) / 100)) * $product->pivot->order_product_quantity;
                                    }, 0);
                                },
                                0
                            ), 2);
                        }
                    }
                    break;
                case 'last month':
                    $month = CarbonPeriod::create(new Carbon('first day of last month'), new Carbon('last day of last month'));
                    foreach ($month as $index => $day) {
                        if ($day->weekOfMonth !== $day->subDay()->weekOfMonth) {
                            $total_revenue_statistic['Quarter ' . ($index / 7 + 1)] = round($seller->orders()->whereBetween('created_at', [date($day), date($day->addDays(7))])->get()->reduce(
                                function ($total_revenue, $order) {
                                    return $total_revenue + $order['products']->reduce(function ($order_revenue, $product) {
                                        return $order_revenue + ($product->price * ((100 - $product->discount) / 100)) * $product->pivot->order_product_quantity;
                                    }, 0);
                                },
                                0
                            ), 2);
                        }
                    }
                    break;
            }
        }

        $response = [
            'total_revenue_statistic' => $total_revenue_statistic,
            'total_orders' => count($orders->get()),
            'total_revenue' =>
            round($seller
                ->orders()
                ->with([
                    'products' => function ($q) use ($seller) {
                        $q->where('seller_id', $seller->id);
                    }
                ])->get()->reduce(
                    function ($total_revenue, $order) {
                        return $total_revenue + $order['products']->reduce(function ($order_revenue, $product) {
                            return $order_revenue + ($product->price * ((100 - $product->discount) / 100)) * $product->pivot->order_product_quantity;
                        }, 0);
                    },
                    0
                ), 2),
            'payment_statistics' => [
                'cash_payment' => count($seller->orders()->where('payment_method', '=', 'cash_payment')->get()),
                'aba_payment' => count($seller->orders()->where('payment_method', '=', 'aba_payment')->get())
            ],
            'earning_today' => round($seller->orders()->whereDate('created_at', date(Carbon::now()))->where('status', 'completed')->get()->reduce(
                function ($total_revenue, $order) {
                    return $total_revenue + $order['products']->reduce(function ($order_revenue, $product) {
                        return $order_revenue + ($product->price * ((100 - $product->discount) / 100)) * $product->pivot->order_product_quantity;
                    }, 0);
                },
                0
            ), 2),
            'in_stock_product' => count($seller->products()->where('quantity', '>', 0)->get()),
            'pending_orders' => count($seller->orders()->where('status', 'pending')->get()),
            'delivered_orders' => count($seller->orders()->where('status', 'delivered')->get()),
            'completed_orders' => count($seller->orders()->where('status', 'completed')->get()),
            'seller' => $seller
        ];

        if ($request->query('exclude')) {
            $exclude = explode(",", $request->query('exclude'));
            $response = array_except($response, $exclude);
        }

        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
