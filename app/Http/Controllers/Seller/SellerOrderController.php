<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Models\Seller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SellerOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Seller $seller)
    {
        // $orders = $seller->orders()->get();
        $orders =
            $seller->orders();

        if ($request->query("date")) {
            switch ($request->query("date")) {
                case "today":
                    $orders = $orders->whereDate('created_at', date(Carbon::today()));
                    break;
                case "this week":
                    $orders = $orders->whereDate('created_at', [date(Carbon::now()->startOfWeek()), date(Carbon::now()->endOfWeek())]);
                    break;
                default:
                    // yyyy-mm-dd
                    $orders = $orders->whereDate('created_at', '=', date($request->query('date')));
            }
        }

        if ($queryStatus = $request->query('status')) {
            $orders = $order->where('status', $queryStatus);
        }

        $orders =  $orders->with([
            'products' => function ($q) use ($seller) {
                $q->where('seller_id', $seller->id);
            }
        ]);

        if ($limit = $request->query('limit')) {
            $orders = $orders->latest()->limit($limit)->get();
        } else {
            $orders = $orders->latest()->paginate(15);
        }

        $orders->map(function ($order) {
            $order['total_cost'] = round($order['products']->reduce(function ($total_cost, $product) {
                return $total_cost + ($product->price * $product->pivot->order_product_quantity);
            }));
            return $order;
        });
        $response =
            [
                'orders' => $orders,
                'revenue' => round($orders->reduce(
                    function ($total_revenue, $order) {
                        return $total_revenue + $order['products']->reduce(function ($order_revenue, $product) {
                            return $order_revenue + ($product->price * ((100 - $product->discount) / 100)) * $product->pivot->order_product_quantity;
                        }, 0);
                    },
                    0
                ), 2)
            ];

        if ($request->query('exclude')) {
            $exclude = explode(",", $request->query('exclude'));
            $response = array_except($response, $exclude);
        }

        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
