<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'seller_id',
        'quantity',
        'price',
        'rating',
    ];

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function product_collections()
    {
        return $this->belongsToMany(ProductCollection::class);
    }
}
